# Raspberry Pi 400 sf keycaps #

This postscript(tm) file print 18 pages containing each a picture for a replacement keycap allowing to change a German keyboard layout to a Swiss-French one on the Raspberry Pi 400

## Usage ##

I'm using Ghostscript to render the images:

`gs -sDEVICE=pngmonod -r180 -g106x106 -sOUTPUTFILE=key%d.png -dNOPAUSE cap.ps -c quit`

The resolution in dpi (`-r180`) and the image size (`-g106x106`) may have to be changed depending on the desired resolutions. Square keycaps are intended to fit a 15x15mm square.

I personally used a Brother P-Touch PT-2730 and the ptouch-print package found at https://github.com/clarkewd/ptouch-print e.g.

`sudo /usr/local/bin/ptouch-print --pad 50 --image key1.png --pad 20 --image key2.png --pad 20 --cutmark`

## Copyright ##

The attached caps.ps file is hereby placed in the public domain.
